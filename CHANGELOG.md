# CHANGELOG

<!--- next entry here -->

## 1.4.0
2020-05-27

### Features

- **notifications:** Add notifications on Microsoft Teams (7fce6b398876af3cf58db100f8814dbbc2b63405)

## 1.3.1
2020-05-26

### Fixes

- **ingress:** Add Discovery ingress (4c70b6a0f463366570f112a4a8355f0e9287b2ad)
- **discovery:** Change zone URL (54e90bfc6b7be466604e4568ccc08f4db18d42bb)

## 1.3.0
2020-05-25

### Features

- **ingress:** Add Discovery interface (56949ca617b022e4317774254d6e32299d03e27b)

## 1.2.5
2020-05-25

### Fixes

- **discovery:** Fix missing port on Eureka URL (617e89f0ebe2e91e4ebee9ac1abc96e195c791e3)

## 1.2.4
2020-05-25

### Fixes

- **env vars:** Change Eureka env var (4361e8f8c6e2f1a36a8197f51817088ef4676616)

## 1.2.3
2020-05-19

### Fixes

- **cors:** Add cors on nginx ingress (b488ea17efa979e393b7af00669b60117a0060eb)

## 1.2.2
2020-05-18

### Fixes

- **admin:** Fix wront container image on deployment.yml (3d9b04e43349e96a220d1959a921831d8525a3cf)

## 1.2.1
2020-05-11

### Fixes

- **deployment:** repair url in Ingress (b9cf7c21f203c252a02574e040238f0bdf3bffe2)

## 1.2.0
2020-05-11

### Features

- **ci:** Add tests on maven package (2d51e3bc5614bffbce02939430a8b1604dd9b8c5)

## 1.1.1
2020-05-11

### Fixes

- **deploy:** Add url in pom (f49b62545effac7b18cd18d39b7277fc1b3888e3)

## 1.1.0
2020-05-11

### Features

- **deploy:** Go on kubernetes (4f8d2ce425338557b7710ee42aec3dbafeb77584)
- **ci:** Auto update version in POM (2c21f49e4be91e52764c4f215c0064b4046b9dc5)

### Fixes

- **config:** bootstraping Discovery config (41437f4b72c281b653d30ce3f025eb1bfbabdf55)
- **deployment:** Change registry url (0452099e62b22182cf2dc95ca553bd921e052513)
- **monitoring:** Expose all actuator routes (61891fe861d4f0e3ba7dc262b97abc9092c146ec)
- **discovery:** Enable discovery client (fa0e833a88c9b43b83f20672bebd44723f13a985)
- **ci:** Repair changelog for ci (4fe489ff475489ece4ce4649393a19a45858de7d)
- **ci:** repair ci (c7342f81fc04a97e840d316af954d5c5df148555)

## 1.0.0
2020-05-11

### Features

- **admin:** Init admin micro-service (d799e04a71761eff6e919f7d1bbc335273c3eccb)
- **deploy:** Go on kubernetes (4f8d2ce425338557b7710ee42aec3dbafeb77584)

### Fixes

- **ci:** Remove postgresql in CI (aa0b6b4d76f9731c1e63b813fd0c0b78eb6aab5f)
- **ci:** add settings for deployment in gitlab (eddd3fdc00f5a6f093e6f6214ebd6650b6edfc36)
- **config:** bootstraping Discovery config (41437f4b72c281b653d30ce3f025eb1bfbabdf55)
- **deployment:** Change registry url (0452099e62b22182cf2dc95ca553bd921e052513)
- **monitoring:** Expose all actuator routes (61891fe861d4f0e3ba7dc262b97abc9092c146ec)
- **discovery:** Enable discovery client (fa0e833a88c9b43b83f20672bebd44723f13a985)
- **ci:** Repair changelog for ci (4fe489ff475489ece4ce4649393a19a45858de7d)

## 1.0.0
2020-05-09

### Features

- **admin:** Init admin micro-service (d799e04a71761eff6e919f7d1bbc335273c3eccb)

### Fixes

- **ci:** Remove postgresql in CI (aa0b6b4d76f9731c1e63b813fd0c0b78eb6aab5f)
- **ci:** add settings for deployment in gitlab (eddd3fdc00f5a6f093e6f6214ebd6650b6edfc36)
- **config:** bootstraping Discovery config (41437f4b72c281b653d30ce3f025eb1bfbabdf55)