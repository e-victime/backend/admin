FROM openjdk:8-jdk-alpine

VOLUME /tmp
ADD /target/admin.jar app.jar

EXPOSE 8383

ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-Dspring.profiles.active=default","-jar","/app.jar"]